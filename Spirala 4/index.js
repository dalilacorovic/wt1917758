const express = require("express");
const bodyParser = require("body-parser");
const app = express(); 
const db = require('./db.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : true}));
app.use(express.static(__dirname + "/public"));

app.get('/', function(req, res){
    res.sendFile(__dirname + '/public/pocetna.html');
});

app.get('/', function(req, res){  
    res.sendFile(__dirname + '/public/rezervacija.html');
});

app.get('/', function(req, res){  
    res.sendFile(__dirname + '/public/osoblje.html');
});

app.get('/', function(req, res){ 
    res.sendFile(__dirname + '/public/sale.html');
});

app.get('/', function(req, res){ 
    res.sendFile(__dirname + '/public/unos.html');
});

app.get('/pozivi.js', function(req, res){
    res.sendFile(__dirname + '/public/pozivi.js');
});

app.get('/pocetna.js', function(req, res){
    res.sendFile(__dirname + '/public/pocetna.js');
});

app.get('/kalendar.js', function(req, res){
    res.sendFile(__dirname + '/public/kalendar.js');
});

app.get('/rezervacija', function(req, result){    
    var dan, semestar, pocetak, kraj, naziv, idTermin, idSale;

    //ucitavanje iz baze zauzeca
    var niz = {periodicna: [], vanredna: []};
    var pObj, vObj;

    db.termin.findAndCountAll().then(async function(array){ 
        var param = array.rows;

        for(let i = 0; i < param.length; i++)
        {
            if(param[i].redovni == true){
                dan = param[i].dan;
                semestar = param[i].semestar;
                pocetak = param[i].pocetak;
                kraj = param[i].kraj;
                idTermin = param[i].id;   

                const arg = await db.rezervacija.findOne({where: {termin: idTermin}});
                    idSale = arg.sala; 
                   
                const arg1 = await db.sala.findOne({where: {id: idSale}})
                    naziv = arg1.naziv; 
                    pObj = {dan: dan, semestar: semestar, pocetak: pocetak, kraj: kraj, naziv: naziv, predavac: "predavac"};
                        
                    niz.periodicna.push(pObj);
            } 
        }

        for(var i = 0; i < param.length; i++)
        {
            if(param[i].redovni == false){
                datum = param[i].datum;
                pocetak = param[i].pocetak;
                kraj = param[i].kraj;
                idTermin = param[i].id;

                const arg = await db.rezervacija.findOne({where: {termin: idTermin}})
                    idSale = arg.sala;

                const arg1 = await db.sala.findAll({where: {id: idSale}});
                    neziv = arg1.naziv;
                    vObj = {datum: datum, pocetak: pocetak, kraj: kraj, naziv: naziv, predavac: 'predavac'};
                    niz.vanredna.push(vObj);
            }
        }

        result.end(JSON.stringify(niz));
    });
});

app.post('/datum/:datum/pocetak/:pocetak/kraj/:kraj/naziv/:naziv/predavac/:predavac/checkbox/:checkbox/period/:period/kolona/:kolona', function(req, res){
    var datum1 = req.params.datum;
    var pocetak1 = req.params.pocetak;
    var kraj1 = req.params.kraj;
    var nazivSale = req.params.naziv;
    var predavac1 = req.params.predavac.split(" ");
    var checkbox = req.params.checkbox; 
    var period = req.params.period;
    var kolona = req.params.kolona;
    var idOsobe, idSale, idTermina;

    //upis podataka u bazu
    //nadjimo id osobe koja rezervise salu
    db.osoblje.findOne({where: {ime: predavac1[0], prezime: predavac1[1]}}).then(function(osoba){
        idOsobe = osoba.id;

        //upisimo u tabelu sale zauzece 
        db.sala.create({naziv: nazivSale, zaduzenaOsoba: idOsobe}).then(function(){
            db.sala.findOne({where: {naziv: nazivSale, zaduzenaOsoba: idOsobe}}).then(function(result){
                idSale = result.id;
    
                //upisimo u tabelu termini zauzece
                if(checkbox == "false"){
                    db.termin.create({redovni: checkbox, dan: null, datum: datum1, semestar: null, pocetak: pocetak1, kraj: kraj1}).then(function(arg){
                        idTermina = arg.dataValues.id;
                        db.rezervacija.create({termin: idTermina, sala: idSale, osoba: idOsobe}).then(function(arg3){
                            res.end();
                        });
                    });
                } else {
                    db.termin.create({redovni: checkbox, dan: kolona, datum: null, semestar: period, pocetak: pocetak1, kraj: kraj1}).then(function(arg){
                        idTermina = arg.dataValues.id;
                           
                        db.rezervacija.create({termin: idTermina, sala: idSale, osoba: idOsobe}).then(function(arg3){
                            res.end();
                        });
                    });
                }
            });
        });
    });
});

app.get('/datum/:datum/pocetak/:pocetak/kraj/:kraj/naziv/:naziv/predavac/:predavac/checkbox/:checkbox/period/:period/kolona/:kolona', function(req, res){
    var datum1 = req.params.datum;
    var pocetak1 = req.params.pocetak;
    var kraj1 = req.params.kraj;
    var nazivSale = req.params.naziv;
    var checkbox = req.params.checkbox; 
    var period = req.params.period;
    var kolona = req.params.kolona;
    var mjesec;
    var idTermina = null, idOsobe = null, idSala = null;
    var istiNaziv = false;

    mjesec = parseInt(datum1.substr(3,5), 10) - 1;

    function izracunajSemestar(mjesec){
        if(mjesec == 0 || mjesec == 1 || mjesec == 10 || mjesec == 11 || mjesec == 9) return "zimski"
        if(mjesec == 2 || mjesec == 3 || mjesec == 4 || mjesec == 5 || mjesec == 6) return "ljetni"
    }

    semestar = izracunajSemestar(mjesec);

    if(checkbox == 'false') {
        db.termin.findOne({where: {redovni: checkbox, dan: null, datum: datum1, semestar: null, pocetak: pocetak1, kraj: kraj1}}).then(function(param){
            if(param == null) res.status(200).end("false");
            else {
                idTermina = param.id;

                db.rezervacija.findOne({where: {termin: idTermina}}).then(function(param){
                    idSala = param.sala;
                    idOsobe = param.osoba;

                    db.sala.findOne({where: {id: idSala}}).then(function(param){
                        if(param.naziv == nazivSale) istiNaziv = true;

                        if(istiNaziv)
                            db.osoblje.findOne({where: {id: idOsobe}}).then(function(param){
                                let predavac1 = param.ime + " " + param.prezime;

                                res.status(404).end(predavac1);
                            });
                        else
                            res.status(200).end("false");
                    });
                });
            }
        });
    } else { 
        db.termin.findAndCountAll().then(async function(arg){
            var redovi = arg.rows;
            pocetak1 += ":00"; kraj1 += ":00";
            for(var i = 0; i < redovi.length; i++){
                kolona = Number(kolona);

                if(redovi[i].redovni == true && redovi[i].dan == kolona && redovi[i].semestar == period && redovi[i].pocetak == pocetak1 && redovi[i].kraj == kraj1){
                    idTermina = redovi[i].id; 

                    const param1 = await db.rezervacija.findOne({where: {termin: idTermina}});
                        idSala = param1.sala;
                        idOsobe = param1.osoba;

                        const param2 = await db.sala.findOne({where: {id: idSala}})
                        if(param2.naziv == nazivSale) istiNaziv = true;

                        if(istiNaziv) {
                            const param3 = await db.osoblje.findOne({where: {id: idOsobe}})
                                predavac1 = param3.ime + " " + param3.prezime;

                                res.status(404).end(predavac1);
                        }
                        else
                            res.status(200).end("false");
                }
            }
            res.status(200).end("false");
        })
    }
});

app.get('/slike/:slike', function(req, res){
    var string = req.params.slike; 
    var image = parseInt(string, 10); 

    if(image != 0)
        image = 3*image; //korak kojim povecamo koju cemo sliku uzeti

    if(image + 1 <= 10) 
        res.write((1 + image) + ".png ");
    
    if(image + 2 <= 10) 
        res.write((2 + image) + ".png ");

    if(image + 3 <= 10) 
        res.write((3 + image) + ".png");

    else
        res.write("");

    res.send();
});

app.get('/osoblje', function(req, res){
    db.osoblje.findAndCountAll().then(function(vjezba){
        var niz = [];  

        if(vjezba == null){
            res.end(JSON.stringify(niz));
        } else {
            var redovi;
            redovi = vjezba.rows;
        for(var i = 0; i < redovi.length; i++)
        {
            var objekat = {ime: redovi[i].ime, prezime: redovi[i].prezime} 
            niz.push(objekat); 
        }
            res.end(JSON.stringify(niz));
        }
    });
});

app.get('/prviDan/:prviDan', function(req, res){
    var pocetak = parseInt(req.params.prviDan, 10);
    var today = new Date();
    var mjesecFormatiraj = today.getMonth() + 1;

    if(mjesecFormatiraj <  10) mjesecFormatiraj = "0" + mjesecFormatiraj;

    var datum = today.getDate() + "." + mjesecFormatiraj + "." + today.getFullYear();
    var vrijeme = today.getHours() + ":" + today.getMinutes();

    var kolonaTrenutnogDana = today.getDate() + pocetak - 1;

    //izracunajmo kolonu datuma radi provjere periodicnog zauzeca
    while(kolonaTrenutnogDana >= 7) kolonaTrenutnogDana -= 7;

    console.log(kolonaTrenutnogDana);

    db.osoblje.findAndCountAll().then(function(vjezba){
        var niz = [];
        var redovi = vjezba.rows;

        for(var i = 0; i < redovi.length; i++)
        {
            let imeOsobe = redovi[i].ime + " " + redovi[i].prezime;
            let pamti = i;

            db.rezervacija.findAll({where: {osoba: redovi[i].id}}).then(function(arg){
                let pozicija; 
                let obj;

                if(arg.length == 0) { 
                    obj = {imeOsobe: imeOsobe, pozicija: "u kancelariji"};
                    niz.push(obj);

                    if(pamti == redovi.length - 1) res.end(JSON.stringify(niz));
                } else {
                    arg.forEach(red => {
                        let idSale = red.sala;
                        let idTermin = red.termin;

                        db.sala.findOne({where: {id: idSale}}).then(function(arg2){
                            pozicija = arg2.naziv; 

                            db.termin.findOne({where: {id: idTermin}}).then(function(arg3){
                             
                                if((arg3.redovni == false && datum == arg3.datum && vrijeme <= arg3.kraj && vrijeme >= arg3.pocetak) || (arg3.redovni == true && kolonaTrenutnogDana == arg3.dan && vrijeme <= arg3.kraj && vrijeme >= arg3.pocetak)){
                                    obj = {imeOsobe: imeOsobe, pozicija: pozicija};
                                    niz.push(obj);

                                    if(pamti == redovi.length - 1) res.end(JSON.stringify(niz));
                                } else {
                                    obj = {imeOsobe: imeOsobe, pozicija: "u kancelariji"};
                                    niz.push(obj);

                                    if(pamti == redovi.length - 1) res.end(JSON.stringify(niz));
                                }
                            });
                        });

                    });
                }
            });
        }
    });
});

app.get('/sale', function(req, res){
    var niz = [];

    db.sala.findAndCountAll().then(function(arg){
        var redovi = arg.rows;

        for(var i = 0; i < redovi.length; i++){
            niz.push({naziv: redovi[i]});
        }

        result.end(JSON.stringify(niz));
    });
});

module.exports = app.listen(8080);