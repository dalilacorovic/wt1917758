var slike = 0;

window.setInterval(function(){
    if (document.getElementById("sadrzaj") != null){
        Pozivi.gdjeJeOsoblje();
    }
}, 30*1000); //pozivamo funkciju gdje je osoblje iz modula Poziv na svakih 30 sekundi
             //30 * 1000 milisekunde
 
let Pozivi = (function(){
    var ajax = new XMLHttpRequest();
    var ajaxDva = new XMLHttpRequest();
    var ajaxTri = new XMLHttpRequest();
    var ajaxCetiri = new XMLHttpRequest();
    var ajaxPeti = new XMLHttpRequest();
    var ajaxSesti = new XMLHttpRequest();
    var zauzeta = null;
    var statickeSlike = new Array(10). fill("");
    var indexSlike = 0;
    var naziviDana = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

    function ucitajJSONPodatke(){
        ajax.onreadystatechange = function(){
            if(ajax.readyState == 4 && ajax.status == 200){
                var niz = JSON.parse(ajax.response); 
               
                Kalendar.ucitajPodatke(niz.periodicna, niz.vanredna);
                Kalendar.iscrtajKalendar(document.getElementById('calendar'), mjesec);
            } 

            if(ajax.readyState == 4 && ajax.status == 404) {
                console.log("greska");
            }
        }
        
        ajax.open("GET", "http://localhost:8080/rezervacija", true);
        ajax.send();
    }  

    function provjeriZauzetost(datumDana){
        var pocetak = document.getElementById("poc").value;
        var kraj = document.getElementById("kraj").value;

        if(!pocetak || !kraj || pocetak > kraj || pocetak == kraj){
            alert("Unesite pocetak i kraj korektno (Neispravan format vremena)!");
        } else {
                //datum mjeseca koji odabiremo
            var datum = datumDana;
            var i = Kalendar.vratiMjesec() + 1;
            var period = Kalendar.mjesecSemestra(i-1);

            if(i < 10) datum += "0" + i + ".2020"
            else datum += i + ".2020";


            var checkbox = document.getElementById("check").checked; 
            var selektorSala = document.getElementById("sale");
            var sala = selektorSala.options[selektorSala.selectedIndex].value; 
            var predavac = "xx";
            var kolonaSelektovanogDana = Kalendar.vratiPocetnuKolonu();
            var brojDana = parseInt(datumDana.substring(0, 2), 10) - 1;

            kolonaSelektovanogDana += brojDana;

                while(kolonaSelektovanogDana >= 7){
                    kolonaSelektovanogDana -= 7;
                }

            ajaxDva.onreadystatechange = function(){ 
                if(ajaxDva.readyState == 4 && ajaxDva.status == 200){  
                    zauzeta = ajaxDva.response;

                    rezervisiDan(datumDana);
                } 

                if(ajaxDva.readyState == 4 && ajaxDva.status == 404) {
                    predavac = ajaxDva.response;
                    alert("Nije moguće rezervisati salu " + sala + " za navedeni datum " + datum + " i terminu od " + pocetak +" do " + kraj +"! Rezervacija: " + predavac);
                }
            }

            ajaxDva.open("GET", "http://localhost:8080/datum/" + datum + "/pocetak/" + pocetak + "/kraj/" + kraj + "/naziv/" + sala + "/predavac/" + predavac + "/checkbox/" + checkbox + "/period/" + period + "/kolona/" + kolonaSelektovanogDana, true);
            ajaxDva.send();
        }
    }

    function rezervisiDan(datumDana){
        var pocetak = document.getElementById("poc").value;
        var kraj = document.getElementById("kraj").value;

        if(!pocetak || !kraj || pocetak > kraj || pocetak == kraj){
            alert("Unesite pocetak i kraj korektno (Neispravan format vremena)!");
        } else {
                //datum mjeseca koji odabiremo
            var datum = datumDana;
            var i = Kalendar.vratiMjesec() + 1;
            var period = Kalendar.mjesecSemestra(i-1);

                if(i < 10) datum += "0" + i + ".2020"
                else datum += i + ".2020";


            var checkbox = document.getElementById("check").checked; 
            var selektorSala = document.getElementById("sale");
            var sala = selektorSala.options[selektorSala.selectedIndex].value;

            let selectOsoblja = document.getElementById("osoblje");

            if(zauzeta == "true" || (checkbox == true && (i == 7 || i == 8 || i == 9))){
                alert("Nije moguće rezervisati salu " + sala + " za navedeni datum " + datum + " i terminu od " + pocetak +" do " + kraj +"!");
            } 
            else if(confirm("Da li zelite rezervisati ovaj termin?")){
                //salji ajax zahtjev za termin
                var predavac = selectOsoblja.options[selectOsoblja.selectedIndex].value;
                var kolonaSelektovanogDana = Kalendar.vratiPocetnuKolonu();
                var brojDana = parseInt(datumDana.substring(0, 2), 10) - 1;

                kolonaSelektovanogDana += brojDana;

                while(kolonaSelektovanogDana >= 7){
                    kolonaSelektovanogDana = kolonaSelektovanogDana - 7;
                }

                ajaxCetiri.onreadystatechange = function(){
                    if(ajaxCetiri.readyState == 4 && ajaxCetiri.status == 200){ 
                        ucitajJSONPodatke();
                    } 
        
                    if(ajaxCetiri.readyState == 4 && ajaxCetiri.status == 404) {
                        alert("Nije moguće rezervisati salu " + sala + " za navedeni datum " + datum + " i terminu od " + pocetak +" do " + kraj +"!");
                    }
                }
            
                ajaxCetiri.open("POST", "http://localhost:8080/datum/" + datum + "/pocetak/" + pocetak + "/kraj/" + kraj + "/naziv/" + sala + "/predavac/" + predavac + "/checkbox/" + checkbox + "/period/" + period + "/kolona/" + kolonaSelektovanogDana, true);
                ajaxCetiri.send();

            }
        }
    }

    function ucitajSlike(slike){
        if(document.getElementById("back").disabled && slike != 0)
            document.getElementById("back").disabled = false;

        ajaxTri.onreadystatechange = function(){
            if(ajaxTri.readyState == 4 && ajaxTri.status == 200){
                var privremenaSlike = ajaxTri.response.split(" ");
                var duzine = 3;                
                if(privremenaSlike.length < 3) document.getElementById("next").disabled = true;

                if(slike != 0) 
                    duzine = duzine + 3*slike; 

                    for(var i = indexSlike, j = 0; i < duzine, j < 3; i++, j++){
                        statickeSlike[i] = privremenaSlike[j];
                    }

                indexSlike = duzine;

                if(privremenaSlike[0] != "")
                    document.getElementById("prva").src = privremenaSlike[0];
                else 
                    document.getElementById("prva").src = "";

                if(privremenaSlike[1] != "")
                    document.getElementById("druga").src = privremenaSlike[1];
                else 
                    document.getElementById("druga").src = "";

                if(privremenaSlike.length == 3 && privremenaSlike[2] != "")
                    document.getElementById("treca").src = privremenaSlike[2];
                else 
                    document.getElementById("treca").src = "";

            } 

            if(ajaxTri.readyState == 4 && ajaxTri.status == 404) {
                console.log("greska");
            }
        }
      
        ajaxTri.open("GET", "http://localhost:8080/slike/" + slike, true);
        ajaxTri.send();
    }

    function vratiSlike(slike){ 

        document.getElementById("next").disabled = false;
        if(slike == 0) document.getElementById("back").disabled = true;
        tmp = indexSlike - 3;
        
        document.getElementById("prva").src = statickeSlike[tmp - 3];
        document.getElementById("druga").src = statickeSlike[tmp - 2];
        document.getElementById("treca").src = statickeSlike[tmp - 1];

        indexSlike = indexSlike - 3; //vratimo index shodno vracanju na prethodni blok slika
    }

    function ucitajOsoblje(){
        let selectOsoblja = document.getElementById("osoblje");

        ajaxPeti.onreadystatechange = function(){
            if(ajaxPeti.readyState == 4 && ajaxPeti.status == 200){
                let obj = JSON.parse(ajaxPeti.response);

                for(var i = 0; i < obj.length; i++){
                    let nazivOsobe = obj[i].ime + " " + obj[i].prezime;

                    let option = document.createElement("option");
                    option.text = nazivOsobe;
                    selectOsoblja.add(option);
                }
            } 

            if(ajaxPeti.readyState == 4 && ajaxPeti.status == 404) {
                console.log("greska");
            }
        }
        
        ajaxPeti.open("GET", "http://localhost:8080/osoblje", true);
        ajaxPeti.send();
    }

    function kolonaPrvogDana() {
        var now = new Date();
        var mjesec = now.getMonth();
        var datumString = new Date(now.getFullYear(), mjesec , 1).toString().substring(0, 3);
  
        for(var i = 0; i <naziviDana.length; i++){
            if (datumString == naziviDana[i]){
                return i;
            }
        }
    }

    function gdjeJeOsoblje(){
        var prviDan = kolonaPrvogDana(); 

        ajaxSesti.onreadystatechange = function(){
            if(ajaxSesti.readyState == 4 && ajaxSesti.status == 200){
                var niz = JSON.parse(ajaxSesti.response);
                console.log(niz);

                //uredi niz objekata 
                //remove sve ovjekte koji se ponavljaju
                for(var i = 0; i < niz.length; i++){
                    for(var j = i + 1; j < niz.length - 1; j++){
                        if(niz[i].imeOsobe == niz[j].imeOsobe && niz[i].pozicija == niz[j].pozicija){
                            niz.splice(j, 1);
                        }

                        if(niz[i].imeOsobe == niz[j].imeOsobe && niz[i].pozicija == "u kancelariji"){
                            niz.splice(i, 1);
                        }

                        if(niz[i].imeOsobe == niz[j].imeOsobe && niz[j].pozicija == "u kancelariji"){
                            niz.splice(j, 1);
                        }
                    }
                }

                var upisHtml = "<table><tr><th>OSOBA</th> <th>TRENUTNA POZICIJA</th></tr>";

                for(var i = 0; i < niz.length; i++){
                    upisHtml += "<tr>";
                    upisHtml += "<td>" + niz[i].imeOsobe + "</td>";
                    upisHtml += "<td>" + niz[i].pozicija + "</td>";
                    upisHtml += "</tr>";
                }

                upisHtml += "</table>";

                document.getElementById("sadrzaj").innerHTML = upisHtml;
            } 

            if(ajaxSesti.readyState == 4 && ajaxSesti.status == 404) {
                console.log("greska");
            }
        }
        
        ajaxSesti.open("GET", "http://localhost:8080/prviDan/" + prviDan, true);
        ajaxSesti.send();
    }

    return{
        ucitajJSONPodatke: ucitajJSONPodatke,
        rezervisiDan: rezervisiDan, 
        ucitajSlike: ucitajSlike,
        vratiSlike: vratiSlike,
        provjeriZauzetost: provjeriZauzetost,
        ucitajOsoblje: ucitajOsoblje,
        gdjeJeOsoblje: gdjeJeOsoblje
    }
}());

