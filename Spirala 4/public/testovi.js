const request = require('supertest');
const app = require("../index");
var povratna = [];
let assert = require('assert');
var testNiz = [{ime: 'Neko', prezime: 'Nekić'}, {ime: 'Drugi', prezime: 'Neko'}, {ime: 'Test', prezime: 'Test'}];

describe('Testiranje serverskih funkcionalnosti', function(){

    //TEST 1
   it ('preuzmi sve osobe iz baze u privatni niz, treba da status = 200', function(done){
        request(app)
            .get("/osoblje")
            .expect(200)
            .end(function(error, result){
                povratna = JSON.parse(result.text);

                //expect(povratna).to.eql([{ime: 'Neko', prezime: 'Nekić'}, {ime: 'Drugi', prezime: 'Neko'}, {ime: 'Test', prezime: 'Test'}]);
                if(error) done(error);
                done();
            });
    }); 

    //TEST 2
    it('Testiraj da li su podaci baze osoblja defaultno definisani', function(){
        assert.deepEqual(povratna, testNiz);
    });     

    //TEST 3
    it('Dohvatanje svih zauzeca', function(){
        request(app)
        .get("/rezervacija")
        .expect({
            periodicna: [
                {
                    dan: 0,
                    semestar: 'zimski',
                    pocetak: '13:00:00',
                    kraj: '14:00:00',
                    naziv: '1-11',
                    predavac: 'predavac'
                }
            ],
            vanredna: [
                {
                    datum: '01.01.2020',
                    pocetak: '12:00:00',
                    kraj: '13:00:00',
                    naziv: '1-15',
                    predavac: 'predavac'
                }
            ]
        })
    });

    //TEST 4
    it('Dodavanje vanrednog novog zauzeca', function(){
        request(app)
        .post("/datum/02.01.2020/pocetak/12:00/kraj/13:00/naziv/0-07/predavac/Test%Test/checkbox/false/period/zimski/kolona/4")
        .expect(200)
    });

    //TEST 5
    it('Dohvatanje svih sala', function(){
        request(app)
        .get("/sale")
        .expect({ naziv: '1-11', naziv: '1-15', naziv: '0-07'}) //jer imamo upis nove sale
    });

    //TEST 6
    it('Provjera da li je upisano novo zauzece', function(){
        request(app)
        .get("/rezervacija")
        .expect({
            periodicna: [
                {
                    dan: 0,
                    semestar: 'zimski',
                    pocetak: '13:00:00',
                    kraj: '14:00:00',
                    naziv: '1-11',
                    predavac: 'predavac'
                }
            ],
            vanredna: [
                {
                    datum: '01.01.2020',
                    pocetak: '12:00:00',
                    kraj: '13:00:00',
                    naziv: '1-15',
                    predavac: 'predavac'
                },
                {
                    datum: '02.01.2020',
                    pocetak: '12:00:00',
                    kraj: '13:00:00',
                    naziv: '0-07',
                    predavac: 'predavac'
                }
            ]
        })
    });

    //TEST 7
    it('Dodavanje redovnog novog zauzeca', function(){
        request(app)
        .post("/datum/02.01.2020/pocetak/12:00/kraj/13:00/naziv/0-07/predavac/Test%Test/checkbox/true/period/zimski/kolona/1")
        .expect(200)
    });

    //TEST 8
    it('Provjera da li je upisano novo zauzece', function(){
        request(app)
        .get("/rezervacija")
        .expect({
            periodicna: [
                {
                    dan: 0,
                    semestar: 'zimski',
                    pocetak: '13:00:00',
                    kraj: '14:00:00',
                    naziv: '1-11',
                    predavac: 'predavac'
                },
                {
                    dan: 1,
                    semestar: 'zimski',
                    pocetak: '13:00:00',
                    kraj: '14:00:00',
                    naziv: '0-07',
                    predavac: 'predavac'
                }
            ],
            vanredna: [
                {
                    datum: '01.01.2020',
                    pocetak: '12:00:00',
                    kraj: '13:00:00',
                    naziv: '1-15',
                    predavac: 'predavac'
                },
                {
                    datum: '02.01.2020',
                    pocetak: '12:00:00',
                    kraj: '13:00:00',
                    naziv: '0-07',
                    predavac: 'predavac'
                }
            ]
        })
    });

    //TEST 9
    it('Dodavanje redovnog zauzeca ne bi trebalo da prodje jer vec imamo prethodno rezervisano - status errora 404', function(){
        request(app)
        .post("/datum/02.01.2020/pocetak/12:00/kraj/13:00/naziv/0-07/predavac/Test%Test/checkbox/true/period/zimski/kolona/1")
        .expect(404)
    });

    //TEST 10
    it('Dodavanje redovnog zauzeca ne bi trebalo da prodje jer vec imamo prethodno rezervisano - status errora 404', function(){
        request(app)
        .post("/datum/02.01.2020/pocetak/12:00/kraj/13:00/naziv/0-07/predavac/Test%Test/checkbox/false/period/zimski/kolona/4")
        .expect(404)
    });
});
