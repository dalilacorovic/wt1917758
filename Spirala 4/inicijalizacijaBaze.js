const db = require('./db.js')

db.sequelize.sync({force: true}).then(function(){
    inicializacija()
    console.log("Gotovo kreiranje tabela i ubacivanje pocetnih podataka!");
});

function inicializacija(){ 

    db.osoblje.create({ime: 'Neko', prezime: 'Nekić', uloga: 'profesor'});
    db.osoblje.create({ime: 'Drugi', prezime: 'Neko', uloga: 'asistent'});
    db.osoblje.create({ime: 'Test', prezime: 'Test', uloga: 'asistent'});
    
    db.sala.create({naziv: '1-11', zaduzenaOsoba: 1}).then(function(arg){
        db.sala.create({naziv: '1-15', zaduzenaOsoba: 2});
    });

    db.termin.create({redovni: false, dan: null, datum: '01.01.2020', semestar: null, pocetak: '12:00', kraj: '13:00'}).then(function(){
        db.termin.create({redovni: true, dan: 0, datum: null, semestar: 'zimski', pocetak: '13:00', kraj: '14:00'}).then(function(){
            //da se izbegnu greske incijalizacije, najprije sacekamo na kreiranje svih stranih kljuceva zaraduspjesnog kreiranja tabele rezervacije

            db.rezervacija.create({termin: 1, sala: 1, osoba: 1}).then(function(){
                db.rezervacija.create({termin: 2, sala: 1, osoba: 3});
            });
        })
    });    
}
