const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19", "root", "root", {host:"127.0.0.1", dialect:"mysql", port: 3308, logging:false});
const db = {};
 
db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.osoblje = sequelize.import( __dirname + '/osoblje.js');
db.rezervacija = sequelize.import( __dirname + '/rezervacija.js');
db.termin = sequelize.import( __dirname + '/termin.js');
db.sala = sequelize.import( __dirname + '/sala.js');


//RELACIJE
//osoblje - jedan na vise - rezervacija
db.osoblje.hasMany(db.rezervacija, {foreignKey:'osoba'});
//db.rezervacija.belongsTo(db.osoblje, {foreignKey: 'osoba'});

//rezervacija - jedan na jedan - termin
db.termin.hasOne(db.rezervacija, {foreignKey: {name: 'termin', unique: true, type: Sequelize.INTEGER}});
//db.rezervacija.belongsTo(db.rezervacija, {foreignKey: {name: 'termin', unique: true, type: Sequelize.INTEGER}});

//rezervacija - vise na jedan - sala
db.sala.hasMany(db.rezervacija, {foreignKey: 'sala'});
//db.rezervacija.belongsTo(db.sala, {foreignKey: 'sala', as: 'ascSala'});

//sala - jedan na jedan - osoblje
db.osoblje.hasOne(db.sala, {foreignKey: 'zaduzenaOsoba'});
//db.sala.belongsTo(db.osoblje, {foreignKey: 'zaduzenaOsoba'});

module.exports = db;